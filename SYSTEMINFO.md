# Amazon Elastic Kubernetes Service

Vendor: Amazon Web Services
Homepage: https://aws.amazon.com/

Product: Elastic Kubernetes Service
Product Page: https://aws.amazon.com/eks/

## Introduction
We classify Amazon Elastic Kubernetes Service into the Cloud domain as it provides managed Kubernetes services on the AWS cloud platform. It simplifies the deployment, management, and scaling of containerized applications using Kubernetes in a cloud environment.

"Amazon Elastic Kubernetes Service (Amazon EKS) is a managed service that makes it easy for you to run Kubernetes on AWS without needing to setup or maintain your own Kubernetes control plane" 

## Why Integrate
The Amazon EKS adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Amazon Elastic Kubernetes Service. With this adapter you have the ability to perform operations such as:

- Get Cluster
- Get Cluster Node Groups
- Update Cluster Config
- Tag Resource

## Additional Product Documentation
The [API documents for Amazon Elastic Kubernetes Service](https://docs.aws.amazon.com/eks/latest/APIReference/Welcome.html)