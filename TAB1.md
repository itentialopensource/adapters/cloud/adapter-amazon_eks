# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Amazon_eks System. The API that was used to build the adapter for Amazon_eks is usually available in the report directory of this adapter. The adapter utilizes the Amazon_eks API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Amazon EKS adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Amazon Elastic Kubernetes Service. With this adapter you have the ability to perform operations such as:

- Get Cluster
- Get Cluster Node Groups
- Update Cluster Config
- Tag Resource

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
