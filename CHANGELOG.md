
## 0.5.8 [11-12-2024]

* more auth changes

See merge request itentialopensource/adapters/adapter-amazon_eks!24

---

## 0.5.7 [10-15-2024]

* Changes made at 2024.10.14_20:34PM

See merge request itentialopensource/adapters/adapter-amazon_eks!23

---

## 0.5.6 [09-30-2024]

* update auth docs

See merge request itentialopensource/adapters/adapter-amazon_eks!21

---

## 0.5.5 [09-12-2024]

* add properties for sts

See merge request itentialopensource/adapters/adapter-amazon_eks!20

---

## 0.5.4 [08-22-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-amazon_eks!19

---

## 0.5.3 [08-14-2024]

* Changes made at 2024.08.14_18:48PM

See merge request itentialopensource/adapters/adapter-amazon_eks!18

---

## 0.5.2 [08-07-2024]

* Changes made at 2024.08.06_20:01PM

See merge request itentialopensource/adapters/adapter-amazon_eks!17

---

## 0.5.1 [08-05-2024]

* Changes made at 2024.08.05_19:17PM

See merge request itentialopensource/adapters/adapter-amazon_eks!16

---

## 0.5.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-amazon_eks!15

---

## 0.4.7 [03-28-2024]

* Changes made at 2024.03.28_13:19PM

See merge request itentialopensource/adapters/cloud/adapter-amazon_eks!14

---

## 0.4.6 [03-15-2024]

* Update metadata.json

See merge request itentialopensource/adapters/cloud/adapter-amazon_eks!13

---

## 0.4.5 [03-13-2024]

* Changes made at 2024.03.13_11:45AM

See merge request itentialopensource/adapters/cloud/adapter-amazon_eks!12

---

## 0.4.4 [03-11-2024]

* Changes made at 2024.03.11_15:33PM

See merge request itentialopensource/adapters/cloud/adapter-amazon_eks!11

---

## 0.4.3 [02-28-2024]

* Changes made at 2024.02.28_11:47AM

See merge request itentialopensource/adapters/cloud/adapter-amazon_eks!10

---

## 0.4.2 [01-27-2024]

* added dynamic region support in stsparam

See merge request itentialopensource/adapters/cloud/adapter-amazon_eks!9

---

## 0.4.1 [12-26-2023]

* update axios and metadata

See merge request itentialopensource/adapters/cloud/adapter-amazon_eks!8

---

## 0.4.0 [12-14-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-amazon_eks!7

---

## 0.3.0 [11-20-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-amazon_eks!7

---
