## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.


### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for EKS. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for EKS.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>


### Specific Adapter Calls

Specific adapter calls are built based on the API of the Amazon Elastic Kubernetes Service. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">describeAddonVersionsSTSRole(kubernetesVersion, maxResults, nextToken, addonName, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the Kubernetes versions that the add-on can be used with.</td>
    <td style="padding:15px">{base_path}/{version}/addons/supported-versions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listClustersSTSRole(maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Lists the Amazon EKS clusters in your AWS account in the specified Region.</td>
    <td style="padding:15px">{base_path}/{version}/clusters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createClusterSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates an Amazon EKS control plane.    The Amazon EKS control plane consists of control plane inst</td>
    <td style="padding:15px">{base_path}/{version}/clusters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteClusterSTSRole(name, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the Amazon EKS cluster control plane.   If you have active services in your cluster that ar</td>
    <td style="padding:15px">{base_path}/{version}/clusters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeClusterSTSRole(name, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns descriptive information about an Amazon EKS cluster.   The API server endpoint and certific</td>
    <td style="padding:15px">{base_path}/{version}/clusters/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAddonsSTSRole(name, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Lists the available add-ons.</td>
    <td style="padding:15px">{base_path}/{version}/clusters/{pathv1}/addons?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAddonSTSRole(name, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates an Amazon EKS add-on.   Amazon EKS add-ons help to automate the provisioning and lifecycle</td>
    <td style="padding:15px">{base_path}/{version}/clusters/{pathv1}/addons?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAddonSTSRole(name, addonName, stsParams, roleName, callback)</td>
    <td style="padding:15px">Delete an Amazon EKS add-on.   When you remove the add-on, it will also be deleted from the cluster</td>
    <td style="padding:15px">{base_path}/{version}/clusters/{pathv1}/addons/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeAddonSTSRole(name, addonName, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes an Amazon EKS add-on.</td>
    <td style="padding:15px">{base_path}/{version}/clusters/{pathv1}/addons/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAddonSTSRole(name, addonName, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Updates an Amazon EKS add-on.</td>
    <td style="padding:15px">{base_path}/{version}/clusters/{pathv1}/addons/{pathv2}/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateEncryptionConfigSTSRole(name, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Associate encryption configuration to an existing cluster.   You can use this API to enable encrypt</td>
    <td style="padding:15px">{base_path}/{version}/clusters/{pathv1}/encryption-config/associate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFargateProfilesSTSRole(name, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Lists the AWS Fargate profiles associated with the specified cluster in your AWS account in the spe</td>
    <td style="padding:15px">{base_path}/{version}/clusters/{pathv1}/fargate-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFargateProfileSTSRole(name, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates an AWS Fargate profile for your Amazon EKS cluster. You must have at least one Fargate prof</td>
    <td style="padding:15px">{base_path}/{version}/clusters/{pathv1}/fargate-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFargateProfileSTSRole(name, fargateProfileName, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes an AWS Fargate profile.   When you delete a Fargate profile, any pods running on Fargate th</td>
    <td style="padding:15px">{base_path}/{version}/clusters/{pathv1}/fargate-profiles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeFargateProfileSTSRole(name, fargateProfileName, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns descriptive information about an AWS Fargate profile.</td>
    <td style="padding:15px">{base_path}/{version}/clusters/{pathv1}/fargate-profiles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listIdentityProviderConfigsSTSRole(name, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">A list of identity provider configurations.</td>
    <td style="padding:15px">{base_path}/{version}/clusters/{pathv1}/identity-provider-configs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateIdentityProviderConfigSTSRole(name, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Associate an identity provider configuration to a cluster.   If you want to authenticate identities</td>
    <td style="padding:15px">{base_path}/{version}/clusters/{pathv1}/identity-provider-configs/associate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeIdentityProviderConfigSTSRole(name, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns descriptive information about an identity provider configuration.</td>
    <td style="padding:15px">{base_path}/{version}/clusters/{pathv1}/identity-provider-configs/describe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disassociateIdentityProviderConfigSTSRole(name, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Disassociates an identity provider configuration from a cluster. If you disassociate an identity pr</td>
    <td style="padding:15px">{base_path}/{version}/clusters/{pathv1}/identity-provider-configs/disassociate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listNodegroupsSTSRole(name, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Lists the Amazon EKS managed node groups associated with the specified cluster in your AWS account</td>
    <td style="padding:15px">{base_path}/{version}/clusters/{pathv1}/node-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNodegroupSTSRole(name, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a managed node group for an Amazon EKS cluster. You can only create a node group for your c</td>
    <td style="padding:15px">{base_path}/{version}/clusters/{pathv1}/node-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNodegroupSTSRole(name, nodegroupName, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes an Amazon EKS node group for a cluster.</td>
    <td style="padding:15px">{base_path}/{version}/clusters/{pathv1}/node-groups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeNodegroupSTSRole(name, nodegroupName, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns descriptive information about an Amazon EKS node group.</td>
    <td style="padding:15px">{base_path}/{version}/clusters/{pathv1}/node-groups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNodegroupConfigSTSRole(name, nodegroupName, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Updates an Amazon EKS managed node group configuration. Your node group continues to function durin</td>
    <td style="padding:15px">{base_path}/{version}/clusters/{pathv1}/node-groups/{pathv2}/update-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNodegroupVersionSTSRole(name, nodegroupName, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Updates the Kubernetes version or AMI version of an Amazon EKS managed node group.   You can update</td>
    <td style="padding:15px">{base_path}/{version}/clusters/{pathv1}/node-groups/{pathv2}/update-version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateClusterConfigSTSRole(name, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Updates an Amazon EKS cluster configuration. Your cluster continues to function during the update.</td>
    <td style="padding:15px">{base_path}/{version}/clusters/{pathv1}/update-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUpdatesSTSRole(name, nodegroupName, addonName, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">Lists the updates associated with an Amazon EKS cluster or managed node group in your AWS account,</td>
    <td style="padding:15px">{base_path}/{version}/clusters/{pathv1}/updates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateClusterVersionSTSRole(name, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Updates an Amazon EKS cluster to the specified Kubernetes version. Your cluster continues to functi</td>
    <td style="padding:15px">{base_path}/{version}/clusters/{pathv1}/updates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeUpdateSTSRole(name, updateId, nodegroupName, addonName, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns descriptive information about an update against your Amazon EKS cluster or associated manag</td>
    <td style="padding:15px">{base_path}/{version}/clusters/{pathv1}/updates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTagsForResourceSTSRole(resourceArn, stsParams, roleName, callback)</td>
    <td style="padding:15px">List the tags for an Amazon EKS resource.</td>
    <td style="padding:15px">{base_path}/{version}/tags/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tagResourceSTSRole(resourceArn, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Associates the specified tags to a resource with the specified  resourceArn . If existing tags on a</td>
    <td style="padding:15px">{base_path}/{version}/tags/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">untagResourceSTSRole(resourceArn, tagKeys, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes specified tags from a resource.</td>
    <td style="padding:15px">{base_path}/{version}/tags/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
